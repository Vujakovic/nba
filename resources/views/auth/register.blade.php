@extends('layouts.app')

@section('title', 'Register')


@section('content')
<div class="container">
    <h2>Register Account</h2>
    <form method="POST" action="/register">

    @csrf
    
        <div class="form-group">
            <label for="name">Name:</label>
            <input  type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{{ old('name') }}">
            @error('name')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="email">Email:</label>
            <input  type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{ old('email') }}">
            @error('email')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input  type="password" class="form-control" id="password" placeholder="Enter password" name="password">
            @error('password')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="password_confirmation">Confirm Password:</label>
            <input  type="password" class="form-control" id="password_confirmation" placeholder="Confirm your password" name="password_confirmation">
            @error('password_confirmation')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>

        
        <button type="submit" class="btn btn-dark">Submit</button>
    </form>
</div>
@endsection
