@extends('layouts.app')

@section('title', 'Login')


@section('content')
<div class="container">
    <h2>Login</h2>
    <form method="POST" action="/login">

    @csrf

        <div class="form-group">
            <label for="email">Email:</label>
            <input  type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{ old('email') }}">
            @error('email')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">Password:</label>
            <input  type="password" class="form-control" id="password" placeholder="Enter password" name="password">
            @error('password')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-dark">Log</button>
    </form>

    <div class="message text-center">
        <span><a href="/register">Still don't have account? Register here.</a> </span>
    </div>


    @if (isset($invalidCredentials) && $invalidCredentials==true)
    <div class="alert alert-danger">
        Invalid credentials
    </div>
    @endif
</div>
@endsection
