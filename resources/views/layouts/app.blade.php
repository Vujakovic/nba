<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>

<!-- Popper JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>


    <title>@yield('title')</title>
</head>
<body>

    @include('partials.nav')

<div class="container-fluid">

    <div class="row">

        <aside class="col-md-2 p-4" style="background-color: rgb(219, 214, 165)">
            @include('partials.sidebar')
        </aside>
        <div class="col-md-10 p-4">
            @include('partials.flash-message')
            @yield('content')
    
        </div>
    </div>

</div>
    
<style>
    aside{
        min-height: 100vh;
    }
</style>

</body>
</html>