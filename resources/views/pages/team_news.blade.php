@extends('layouts.app')



@section('title', $teamNews->name)
    
@section('content')

<h3>{{$teamNews->name}}  news:</h3>
<div class="news">
    
         @foreach ($teamNews->news as $single)
             <div class="card m-1 p-2">
                 <h4><a href="/news/{{$single->id}}">{{$single->title}}</a></h4>
                 <small>Author: <strong>{{$single->user->name}}</strong></small>
                 <small>{{$single->created_at}}</small>
                 <p>
                     {{Str::limit($single->content, 100)}}
                 </p>
             </div>
         @endforeach
     
 </div>
    
@endsection