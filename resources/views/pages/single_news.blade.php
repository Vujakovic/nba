@extends('layouts.app')


@section('title', $article->title)
    
@section('content')
    <h1>{{$article->title}}</h1>
    <small>Author: <strong>{{$article->user->name}}</strong></small>
    <hr>
    <p>
        {{$article->content}}
    </p>
<hr>

<h5>News refers to:</h5>
    <ul>
        @foreach ($article->teams as $team)
            <li><a href="/teams/{{$team->id}}">{{$team->name}}</a></li>
        @endforeach
    </ul>
@endsection