@extends('layouts.app')

@section('title', 'Add new article')
    
@section('content')
    


<form class="mt-5" method="POST" action="/news">
    @csrf

    <div class="form-group">

        <input class="form-control" name="title" placeholder="Title" value="{{ old('title') }}" class="@error('title') alert-danger @enderror" /><br />
        @error('title')
        <div class="alert alert-danger">
            {{$message}}
        </div>
        @enderror
    </div>

    <div class="form-group">

        <textarea  name="content" placeholder="Enter news..." class="form-control @error('content') alert-danger @enderror">{{ old('body') }}</textarea><br />
        @error('content')
        <div class="alert alert-danger">
            {{$message}}
        </div>
        @enderror
    </div>


    <div>
        <p>
            Select teams:
        </p> 
            @foreach ($teams as $team)
                <div class="form-check form-group">
                    <input name="teams[]" type="checkbox" class="form-check-input" id="team-{{$team->id}}" value="{{$team->id}}">
                    <label  for="team-{{$team->id}}" class="form-check-label">{{$team->name}}</label>
                </div>
            @endforeach
        
    </div>


    <br />

    <button class="btn btn-success" type="submit">Create a post</button>
</form>

@endsection