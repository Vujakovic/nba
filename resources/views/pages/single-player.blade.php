@extends('layouts.app')

@section('title', $player->first_name)
    

@section('content')

    <span>Player:</span>
    <h1>{{$player->first_name}} {{$player->last_name}}</h1>
    <span><small>Email:</small>  <strong>{{$player->email}}</strong></span>

    <span class="d-block">
        Current Team: <strong><a href="/teams/{{$player->team->id}}">{{$player->team->name}}</a></strong>
    </span>
@endsection
