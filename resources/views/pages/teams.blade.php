@extends('layouts.app')

@section('title', 'All teams')
    


@section('content')
    <h1>All teams list</h1>

    <div class="teams">
        @foreach ($teams as $team)
            <div class="team card m-2 p-2">
                <h5><a href="/teams/{{$team->id}}">{{$team->name}}</a></h5>
                <small><a href="/news/team/{{$team->name}}">News</a> </small>
            </div>
        @endforeach
    </div>
@endsection
