@extends('layouts.app')


@section('title', 'News')
    
@section('content')
    <div class="news">
       @if (count($news)>0)
            @foreach ($news as $single)
                <div class="card m-1 p-2">
                    <h4><a href="/news/{{$single->id}}">{{$single->title}}</a></h4>
                    <small>Author: <strong>{{$single->user->name}}</strong></small>
                    <small>{{$single->created_at}}</small>
                    <p>
                        {{Str::limit($single->content, 100)}}
                    </p>
                </div>
            @endforeach
       @else
           <p>There is no news</p>
       @endif
        
    </div>
    <div class="pagination mt-5">
        {{ $news->links()}}
    </div>

    <style>
    
        svg{
            width:30px;
        }
    </style>
@endsection