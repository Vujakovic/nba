@extends('layouts.app')

@section('title', )
    
@section('content')

<div class="team-details">
    <h1>{{$team->name}}</h1>
    <span class="d-block">Email: <strong>{{$team->email}}</strong> </span>
    <span class="d-block">Address: <strong>{{$team->address}}</strong> </span>
    <span class="d-block">City: <strong>{{$team->city}}</strong> </span>

</div>


<div class="player-list">
    <ul>
        @foreach ($team->players as $player)
            <li><a href="/players/{{$player->id}}">{{$player->first_name}} {{$player->last_name}}</a> </li>
        @endforeach
    </ul>
</div>

<div class="news">
    <h5>{{$team->name}} news:</h5>
    <ul>
        @foreach ($team->news as $news)
            <li><a href="/news/{{$news->id}}">{{$news->title}}</a></li>
        @endforeach
    </ul>
</div>

<hr>

<div class="comments">
    <h2>Comments:</h2>

    <form action="/teams/{{$team->id}}" method="post">
        @csrf

        <div class="form-group">
            <label for="body">Add new comment:</label>
            <textarea name="body" class="form-control" id="body" cols="30" rows="5">{{old('body')}}</textarea>
            <button type="submit" class="btn btn-success mt-4">Add Comment</button>
            @error('body')
                <div class="alert alert-danger" role="alert">
                {{ $message }}
              </div>
            @enderror
        </div>
    </form>
    <hr>

    <div class="team-comments">
        @foreach ($team->comments as $comment)
            <div class="card m-2 p-2 bg-secondary text-white">
                <small>{{$comment->created_at}}</small>
                <p>{{$comment->body}}</p>
            </div>
        @endforeach
    </div>
</div>

@endsection