<nav class="navbar navbar-expand navbar-dark bg-dark text-white">
    <a class="navbar-brand" href="/">NBA app</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02"
        aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExample02">
        <ul class="navbar-nav mr-auto">
        @auth
            <li class="nav-item">
              <a class="nav-link" href="/">All teams</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/news">News</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/news/create">Add News</a>
        </li>
          @endauth
          @guest
              <li class="nav-item">
                  <a class="nav-link" href="/register">Register</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" href="/login">Log In</a>
              </li>
          @endguest
        </ul>


        @auth
        <strong>{{Auth::user()->name}}</strong>
        <form method="POST" action="/logout" class="form-inline my-2 my-lg-0 ml-2">
            @csrf
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
        </form>
        @endauth
    </div>
</nav>
