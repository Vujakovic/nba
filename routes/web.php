<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\TeamsController;
use App\Http\Controllers\PlayerController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\NewsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/







//AUTH
Route::middleware(['guest'])->group(function () {
    Route::get('/register', [AuthController::class,'getRegisterPage']);
    Route::post('/register', [AuthController::class,'registerUser']);
    Route::get('/login', [AuthController::class,'getLoginPage'])->name('login');
    Route::post('/login', [AuthController::class,'login']);
});

Route::middleware(['auth'])->group(function () {
    //TEAMS
    Route::get('/',[TeamsController::class, 'index']);
    Route::get('/teams/{id}', [TeamsController::class, 'show']);

    //PLAYERS
    Route::get('/players/{id}', [PlayerController::class, 'show']);

    Route::post('/logout', [AuthController::class, 'logout']);

 //COMMENTS
 Route::post('/teams/{team_id}', [CommentsController::class, 'store'])->middleware(['blockOffensive','auth']);
});

Route::get('/forbidden', [CommentsController::class, 'blockOffensive'])->name('forbidden');

Route::get('/verifymail/{user_id}', [AuthController::class, 'verifymail']);

//NEWS
Route::get('/news', [NewsController::class, 'index']);
Route::get('/news/create', [NewsController::class, 'create']);
Route::post('/news', [NewsController::class, 'store']);
Route::get('/news/{id}', [NewsController::class, 'show']);
Route::get('/news/team/{team_name}', [NewsController::class, 'newsByTeam']);


Auth::routes(['verify' => true]);






Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('verified');
