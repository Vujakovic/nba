<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterUserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyMail;
use Illuminate\Auth\Events\Registered;

class AuthController extends Controller
{
    public function getRegisterPage()
    {
        return view('auth.register');
    }

    public function registerUser(RegisterUserRequest $request)
    {
        $data = $request->validated();
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);

    
        event(new Registered($user));

        if($user->email_verified_at!==null){
            Auth::login($user);
    
        }
        
        return redirect('login');
        
    }

    public function getLoginPage()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            return redirect('/');
        }else{
            return view('auth.login', ['invalidCredentials' => true]);
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function verifymail($user_id)
    {
       
    }
}
