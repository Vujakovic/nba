<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Team;

class TeamsController extends Controller
{

    public function index()
    {
        $teams = Team::all();
        return view('pages.teams', [
            'teams'     => $teams
        ]);
    }

    public function show($id)
    {   
        $team = Team::with(['players','comments'])->findOrFail($id);
        
        return view('pages.single-team', [
            'team'  => $team,
        ]);
    }


}
