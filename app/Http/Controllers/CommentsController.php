<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateCommentRequest;
use App\Models\Team;
use Illuminate\Support\Facades\Auth;
use App\Models\Comment;
use Illuminate\Support\Facades\Mail;
use App\Mail\CommentReceived;

class CommentsController extends Controller
{

    public function store($team_id, CreateCommentRequest $request)
    {
        $data = $request->validated();
        
        $user_id = Auth::id();
        // $data['user_id']= $user_id;
        $team= Team::findOrFail($team_id);
        Comment::create(array_merge($data, ['user_id'=> $user_id, 'team_id'=> $team_id]));
        // $team->comments()->create($data);
        
        Mail::to($team->email)->send(new CommentReceived($team));
        return back();
    }

    public function blockOffensive()
    {
        return view('pages.forbidden-comment');
    }
}
